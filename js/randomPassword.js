var symbols = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
var digits = "0123456789";
var lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
var uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var minLength = 15;
var maxLength = 94;

var typesArray = ["symbol", "digit", "uppercaseLetter", "lowercaseLetter"];
var typesArrayNoSymbol = ["digit", "uppercaseLetter", "lowercaseLetter"];

function stringInit() {
    symbols = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
    digits = "0123456789";
    lowercaseLetters = "abcdefghijklmnopqrstuvwxyz";
    uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return;
}

function myRand(a, b) {
    var min = Math.min(a, b);
    var max = Math.max(a, b);
    var randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
    return randomNumber;
}

//Checks if value is between a and b
function isBetween(value, a, b) {
    var min = Math.min(a, b);
    var max = Math.max(a, b);
    return (min <= value) && (value <= max);
}

function getType(character) {
    var asciiCode = character.charCodeAt(0);

    if (isBetween(asciiCode, 48, 57)) {
        t = "digit";
    } else if (isBetween(asciiCode, 65, 90)) {
        t = "uppercaseLetter";
    } else if (isBetween(asciiCode, 97, 122)) {
        t = "lowercaseLetter";
    } else {
        t = "symbol";
    }

    return t;
}

//In order to prevent two consecutive characters of the same type
function getRandomType(excludedType, noSymbols) {
    var randomType = excludedType;
    var randomIndex, types, isEmpty;

    if (noSymbols) {
        types = typesArrayNoSymbol;
    } else {
        types = typesArray;
    }

    while (randomType == excludedType) {
        randomIndex = myRand(0, types.length - 1);
        randomType = types[randomIndex];

        switch (randomType) {
            case "uppercaseLetter":
                isEmpty = (uppercaseLetters.length == 0)
                break;

            case "lowercaseLetter":
                isEmpty = (lowercaseLetters.length == 0)
                break;

            case "digit":
                isEmpty = (digits.length == 0)
                break;

            default:
                isEmpty = (symbols.length == 0)
                break;
        }

        if (isEmpty) {
            randomType = excludedType;
        }
    }

    return randomType;
}

function getRandomChar(type, noRepeat) {
    var str, randomIndex, randomChar;

    switch (type) {
        case "uppercaseLetter":
            str = uppercaseLetters;
            break;
        case "lowercaseLetter":
            str = lowercaseLetters;
            break;
        case "digit":
            str = digits;
            break;
        default:
            str = symbols;
            break;
    }

    randomIndex = myRand(0, str.length - 1);
    randomChar = str.charAt(randomIndex);

    if (noRepeat) {
        switch (type) {
            case "uppercaseLetter":
                uppercaseLetters = uppercaseLetters.replace(randomChar, "");
                break;
            case "lowercaseLetter":
                lowercaseLetters = lowercaseLetters.replace(randomChar, "");
                break;
            case "digit":
                digits = digits.replace(randomChar, "");
                break;
            default:
                symbols = symbols.replace(randomChar, "");
                break;
        }
    }

    return randomChar;
}

function buildPassword(length, noSymbols, noRepeat) {
    defaultMessage();
    stringInit();
    var password = "";

    var randomType = getRandomType("unknown", noSymbols);
    var randomChar = getRandomChar(randomType, noRepeat);
    password += randomChar;

    var previousChar, previousCharType;

    if (noSymbols && noRepeat && (length > 62)) {
        length = 62;
    } else if (length > maxLength) {
        noRepeat = false;
    } else if (length < minLength) {
        length = minLength;
    }

    while (password.length < length) {
        previousChar = password.charAt(password.length - 1);
        previousCharType = getType(previousChar);

        randomType = getRandomType(previousCharType, noSymbols);
        randomChar = getRandomChar(randomType, noRepeat);

        password += randomChar;
    }

    return password;
}

function addPasswordToList(password) {
    var ul = document.getElementById("passwordList");
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(password));
    ul.appendChild(li);
    return;
}

function fillPasswordInput() {
    var length = document.getElementById("passwordLength").value;
    var noSymbols = document.getElementById("noSymbols").checked;
    var noRepeat = document.getElementById("noRepeat").checked;
    var password = buildPassword(length, noSymbols, noRepeat);

    document.getElementById("password").value = password;
    addPasswordToList(password);
    return;
};


function copyToClipboard() {

    /* Get the text field */
    var password = document.getElementById("password");
    var copiedClipboard = document.getElementById("copyClipboard");

    /* Select the text field */
    password.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");

    copySuccess();
    return;
}

function randomLength() {
    var randomlength = myRand(15, maxLength);
    document.getElementById("passwordLength").value = randomlength;
    return;
}

function clearPasswordList() {
    var passwordList = document.querySelectorAll("#passwordList > li:not(:first-child)");
    var i;

    for (i = 0; i < passwordList.length; i++) {
        passwordList[i].innerHTML = "";
    }

    return;
}