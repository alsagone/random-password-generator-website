$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

function changeTooltipMessage(message) {
    $('[data-toggle="tooltip"]').attr('data-original-title', message);
    return;
}

function defaultMessage() {
    changeTooltipMessage("Copy to clipboard");
    return;
}

function copySuccess() {
    changeTooltipMessage("Successfuly copied to clipboard");
    return;
}