function getGeneratedPasswords() {
    var passwords = [];
    var elementsLI = document.getElementsByTagName("li");
    var length = elementsLI.length;
    var i;

    for (i = 1; i < length; i++) {
        passwords.push(elementsLI[i].innerHTML + "\n");
    }

    return passwords;
}

function generateFilename() {
    var today = new Date();
    var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var dateTime = date + "_" + time;

    var filename = "generatedPasswords_" + dateTime + ".txt";
    return filename;
}

function createPasswordBlob() {
    var passwordArray = getGeneratedPasswords();
    var blob = new Blob(passwordArray, { type: "text/plain;charset=utf-8" });
    return blob;
}

function savePasswords(blob) {
    var filename = generateFilename();
    saveAs(blob, filename);
    return;
}

function downloadPasswords() {
    var blob = createPasswordBlob();
    savePasswords(blob);
    return;
}